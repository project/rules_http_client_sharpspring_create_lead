<?php

namespace Drupal\rules_http_client_sharpspring_create_lead\Plugin\RulesAction;

use Drupal\rules\Core\RulesActionBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides "Rules Http client SharpSpring" rules action.
 *
 * @RulesAction(
 *   id = "rules_http_client_ss",
 *   label = @Translation("Request HTTP data Sharpspring"),
 *   category = @Translation("Data"),
 *   context = {
 *     "url" = @ContextDefinition("string",
 *       label = @Translation("URL"),
 *       description = @Translation("Url address where to post, get and delete request send."),
 *       required = TRUE,
 *       assignment_restriction = "data",
 *     ),
 *      "account_id" = @ContextDefinition("string",
 *       label = @Translation("Sharpspring Account ID"),
 *       description = @Translation("Account ID sharpspring"),
 *       required = TRUE,
 *     ),
 *       "secret_key" = @ContextDefinition("string",
 *       label = @Translation("Sharpspring Secret Key"),
 *       description = @Translation("Secret Key SharpSpring"),
 *       required = TRUE,
 *     ),
 *     "headers" = @ContextDefinition("string",
 *       label = @Translation("Headers"),
 *       description = @Translation("Request headers to send as 'name: value' pairs, one per line (e.g., Accept: text/plain). See <a href='https://www.wikipedia.org/wiki/List_of_HTTP_header_fields'>wikipedia.org/wiki/List_of_HTTP_header_fields</a> for more information."),
 *       required = FALSE,
 *      ),
 *     "method" = @ContextDefinition("string",
 *       label = @Translation("Method"),
 *       description = @Translation("The HTTP request methods like'HEAD','POST','PUT','DELETE','TRACE','OPTIONS','CONNECT','PATCH' etc."),
 *       required = FALSE,
 *     ),
 *     "method_ss" = @ContextDefinition("string",
 *       label = @Translation("Method API SharpSpring"),
 *       description = @Translation("Method API Sharspring"),
 *       required = FALSE,
 *     ),
 *     "first_name" = @ContextDefinition("email",
 *       label = @Translation("First Name"),
 *       description = @Translation("First Name"),
 *       required = FALSE,
 *     ),
 *
 *     "email_address" = @ContextDefinition("email",
 *       label = @Translation("Email Address"),
 *       description = @Translation("Email Address"),
 *       required = FALSE,
 *     ),
 *   },
 *   provides = {
 *     "http_response" = @ContextDefinition("string",
 *       label = @Translation("HTTP data")
 *     )
 *   }
 * )
 *
 * @todo: Define that message Context should be textarea comparing with textfield Subject
 * @todo: Add access callback information from Drupal 7.
 */
class RulesHttpClientSS extends RulesActionBase implements ContainerFactoryPluginInterface {

  /**
   * The logger for the rules channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a httpClient object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerChannelFactoryInterface $logger_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->logger = $logger_factory->get('rules_http_client');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory')
    );
  }

  /**
   * Send a system sharpspring.
   *
   * @param string[] $url
   *   Url addresses HTTP request.
   * @param string[] $account_id
   *   Account id SharpSpring.
   * @param string[] $secret_key
   *   Secret Key SharpSpring.
   * @param string|null $headers
   *   (optional) Header information of HTTP Request.
   * @param string $method
   *   (optional) Method of HTTP request.
   * @param string $method_ss
   *   (optional) Method  used in API SharpSpring
   * @param string|null $data
   *   (optional) Raw data of HTTP request.
   * @param int|null $maxRedirect
   *   (optional) Max redirect for HTTP request.
   * @param int|null $timeOut
   *   (optional) Time Out for HTTP request.
   */
  protected function doExecute($url, $account_id, $secret_key, $headers, $method, $method_ss, $first_name, $email_address) {
    // Headers section.
    $headers = explode("\r\n", $headers);
    if (is_array($headers)) {
      foreach ($headers as $header) {
        if (!empty($header) && strpos($header, ':') !== FALSE) {
          list($name, $value) = explode(':', $header, 2);
          if (!empty($name)) {
            $options['headers'][$name] = ltrim($value);
          }
        }
      }
    }

    $requestID = session_id();

    $data = array(
      'method' => $method_ss,
      'params' => array('firstName' => $first_name, 'emailAddress' => $email_address),
      'id' => $requestID,
   );

    $data = json_encode($data);

    $queryString = http_build_query(array('accountID' => $account_id, 'secretKey' => $secret_key));
    $url = "$url?$queryString";

    $curl = curl_init();
    curl_setopt_array($curl, [
      CURLOPT_CUSTOMREQUEST => $method,
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_POSTFIELDS => $data,
      CURLOPT_HTTPHEADER => ['Content-Type: application/json', 'X-CSRF-Token: yJeocNuElZq6shl7veWUMA8lSof0nKxWWf1UKPL5IrE'],
      CURLOPT_URL => $url,
      CURLOPT_SSL_VERIFYPEER => FALSE,
    ]);
    $response = curl_exec($curl);
    $output = json_decode($response);

    $this->logger->warning('API return for %email_address  %message', ['%message' => var_export($output,TRUE), '%email_address' => $email_address]);

  }

}
